﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
    static class Program
    {
        static void Main(string[] args)
        {
            /// Use this method to implement tasks
        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            int num;
            if(int.TryParse(source, out num))
            {
                return num;
            }
            else
            {
                throw new ArgumentException("error");
            }
        }

        public int Multiplication(int num1, int num2)
        {
            int p = 0;
            for (int i = 0; i < Math.Abs(num2); i++)
            {
                p += Math.Abs(num1);
            }
            if ((num1 < 0 && num2 < 0) || (num1 > 0 && num2 > 0))
            {
                return p;
            }
            else
            {
                return -p;
            }
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            int num;
            if (!int.TryParse(input, out num) || num<0)
            {
                result = 0;
                return false;
            }
            else
            {
                result = 1;
                return true;
            }
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < naturalNumber; i++)
            {
                result.Add(i * 2);
            }
            return result;
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            int num;
            if (!int.TryParse(input, out num) || num<0)
            {
                result = 0;
                return false;
            }
            else
            {
                result = 1;
                return true;
            }
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            string res = Convert.ToString(source);
            string dTR = Convert.ToString(digitToRemove);
            int i = 0;
            while (i < res.Length)
            {
                if (res[i] == dTR[0])
                {
                    res = res.Remove(i, 1);
                    continue;
                }
                i++;
            }
            return res;
        }
    }
}
